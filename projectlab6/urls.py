"""projectlab6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from applab6.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', landingpage),
    path('isistatus', isistatus, name = "isistatus"),
    path('profil/', profil),
    path('listBuku/', bookList , name="listBuku"),
    path('fileJson/', fileJson),
    path('subscribe/', subscribe),
    path('available_email/', available_email, name="available_email" ),
    path('subscribe_page/', subscribe_page, name="subscribe_page" ),
    path('list_subscribe/', list_subscribe, name="list_subscribe" ),
    path('list_subs/', list_subs, name="list_subs" ),
    # path('del_subs/', del_subs, name="del_subs" ),
    path('auth/' , include('social_django.urls', namespace='social')),
    path('logout/', logoutView, name ="logout"),
    path('tambah/', tambah, name="tambah"),
    path('kurang/', kurang, name="kurang"),
    path('bookList/', bookList, name="bookList"),
    

] 

