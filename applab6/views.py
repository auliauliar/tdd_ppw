from django.shortcuts import render
from .forms import Form_Status
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import requests
from .models import createstatus, Subscribe
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers


response = {}
response['counter'] = 0
# Create your views here.
def landingpage(request):
	all_stats = createstatus.objects.all()
	response['all_stats'] = all_stats
	response['forms'] = Form_Status()
	return render(request, 'halaman1.html', response)

def isistatus(request):
	form = Form_Status(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['message'] = request.POST['message']
		stats = createstatus(message=response['message'])
		stats.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')
	
def profil (request):
	return render(request, 'halaman2.html', response)

def listBuku (request):
	return render(request, 'halaman3.html', response)

def fileJson(request):
	txt = request.GET.get('find', 'quilting')
	url = "https://www.googleapis.com/books/v1/volumes?q=" + txt
	data = requests.get(url).json()
	return JsonResponse(data)

def subscribe(request):
	return render(request, 'halaman4.html', response)


def subscribe_page(request):
	if(request.method == "POST"):
		data = dict(request.POST.items())
		tamu = Subscribe(nama = data['name'], email = data['email'],password = data['password'])
		try:
			tamu.full_clean()
			tamu.save()
		except Exception as e:
			print(e.message_dict.items())
			e = [v[0] for k, v in e.message_dict.items()]
			return JsonResponse({"error": e}, status = 400)
		return JsonResponse({"message":"Data berhasil disimpan"})
	else:
		return render(request, 'halaman4.html', {})

def available_email(request):
	email = request.GET["email"]
	emails = Subscribe.objects.filter(email=email).all()
	return JsonResponse({"email": email, "available": not bool(emails)})

def list_subscribe(request):
	data = Subscribe.objects.all().values('nama','email','password')
	user_list = list(data)
	return JsonResponse(user_list, safe = False)

def list_subs(request):
	return render(request, 'halaman5.html', response)


# def del_subs(request):
#     if 'email' in request.POST:
#         print('yokyokkk')
#         email = request.POST['email']
#         lele = Subscribe.objects.filter(email=email)
#         lele.delete()
#         return HttpResponseRedirect("/list_subs")
#     else:
#         print('helppp')
#         return HttpResponseRedirect("/list_subs")


def logoutView(request):
	request.session.flush()
	print(dict(request.session))
	logout(request)
	return HttpResponseRedirect('/listBuku')

def bookList(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		print(dict(request.session))
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']

		print (dict(request.session))

		
	else:
		if 'counter' not in request.session:
			request.session['counter'] = 0
		response['counter'] = request.session['counter']
	return render(request, "halaman3.html", response)

@csrf_exempt
def tambah(request):
	if request.user.is_authenticated:
		print(dict(request.session))
		request.session['counter'] = request.session['counter'] + 1
		print(dict(request.session))
	else:
		pass
	return HttpResponse(request.session['counter'], content_type = 'application/json')


@csrf_exempt
def kurang(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = 'application/json')