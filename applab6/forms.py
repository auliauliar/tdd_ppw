from django import forms 



class Form_Status(forms.Form): 
	attrs = {
		'type' : 'text',
		'class' : 'form-control',
		'placeholder' : 'Isi Status',
		'id' : 'id_message',
	}
	message = forms.CharField(label='', required=True, max_length=300, widget=forms.TextInput(attrs = attrs))
