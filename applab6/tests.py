from django.test import TestCase
from django.test import Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import *
from .forms import *

# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = createstatus.objects.create(message = "data base PPW")

        # Retrieving all available activity
        counting_all_available_todo = createstatus.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = Form_Status()
        self.assertIn('class="form-control', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Form_Status(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/isistatus', {'message': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/') 
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_landing_page_has_greeting(self):
        response = Client().get('/')
        landing_page_content = 'Hello, Apa kabar?'
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content, html_response)

    def test_lab_6_html2_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200) 

    def test_lab6_using_template(self):
        response = Client().get('/profil/')
        self.assertTemplateUsed(response, 'halaman2.html')
    
    def test_nama_profil (self):
        response = Client().get('/profil/')
        profil_page = 'Aulia Ramadhani'
        html_response = response.content.decode('utf8')
        self.assertIn(profil_page, html_response)

    def test_halaman_fileJson (self):
        response = Client().get('/fileJson/')
        self.assertEqual(response.status_code, 200) 


class Lab7FunctionalTest(TestCase):

    # def setUp(self):
    #     chrome_options = Options()
    #     self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
    #     super(Lab7FunctionalTest, self).setUp()

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Lab7FunctionalTest,self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        isistatus = selenium.find_element_by_id('id_message')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        isistatus.send_keys('Coba Coba')
        
        # submitting the form
        submit.send_keys(Keys.RETURN)

        # check status on page
        self.assertIn('Coba Coba', selenium.page_source)

    # def test_web_background_is_pink(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('https://isi-status.herokuapp.com')
    #     body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
    #     self.assertEqual(body, "lavenderblush")
    
    def test_pagesource(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://isi-status.herokuapp.com')
        self.assertIn('Hello, Apa kabar?' , selenium.page_source)

    def test_title(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://isi-status.herokuapp.com')
        self.assertIn('Isi Status' , selenium.title)

    def test_web_background_is_lavenderblush(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://isi-status.herokuapp.com')
        body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertEqual(body, "rgba(255, 240, 245, 1)")

    def test_class_table_style(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://isi-status.herokuapp.com')
        style = selenium.find_element_by_id('submit').value_of_css_property('float')
        self.assertEqual(style, "none")

    def test_page_Profil(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://isi-status.herokuapp.com/profil/')
        self.assertIn('Aulia Ramadhani' , selenium.page_source)

    

